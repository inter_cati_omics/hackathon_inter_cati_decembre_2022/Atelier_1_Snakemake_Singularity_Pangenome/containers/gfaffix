# gfaffix Singularity container
### Package gfaffix Version 0.1.4
GFAffix identifies and collapses walk-preserving shared affixes in variation graphs

Homepage:

https://github.com/marschall-lab/GFAffix

Package installation using Miniconda3 V4.11.0
All packages are in /opt/miniconda/bin & are in PATH
gfaffix Version: 0.1.4<br>
Singularity container based on the recipe: Singularity.gfaffix_v0.1.4.def

Local build:
```
sudo singularity build gfaffix_v0.1.4.sif Singularity.gfaffix_v0.1.4.def
```

Get image help:
```
singularity run-help gfaffix_v0.1.4.sif
```

Default runscript: gfaffix<br>
Usage:
```
./gfaffix_v0.1.4.sif --help
```
or:
```
singularity exec gfaffix_v0.1.4.sif gfaffix --help
```

image singularity (V>=3.3) is automaticly built (gitlab-ci) and pushed on the registry using the .gitlab-ci.yml <br>
can be pull (singularity version >=3.3) with:<br>

```
singularity pull gfaffix_v0.1.4.sif oras://registry.forgemia.inra.fr/inter_cati_omics/hackathon_inter_cati_decembre_2022/atelier_1_snakemake_singularity_pangenome/containers/gfaffix/gfaffix:latest

```

